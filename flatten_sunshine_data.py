print("hello")
import csv
import os

base_dir = '/Users/calhamd/Documents/sunshine/'
write_dir =  '/Users/calhamd/Documents/sunshine_flattened/'
files = os.listdir(base_dir)

headers = ['Station', 'Date(NZST)', 'Amount(Hrs)', 'Period(Hrs)', 'Freq', 'StationName', 'Latitude','Longitude', 'Height', 'NetworkNumber', 'OriginalFileIdentifier']
with open(write_dir+'nz_flattened.csv', mode='w') as flattened_file:
    csv_writer = csv.writer(flattened_file, delimiter=',')
    csv_writer.writerow(headers)    

    for file in files:        
        full_file=base_dir+file    
        with open(full_file) as csv_file:
            #line = csv_file.readline()
            csv_reader = csv.reader(csv_file, delimiter=',', quotechar='|')
            #read station lookups        
            station_lookup = {}
            for row in csv_reader:
                if len(row)>0 and row[0] =='Station information:':
                    continue
                if len(row)>0 and row[0] =='Name':
                    continue
                if len(row)>0 and row[0].startswith('Note'):
                    break
                for i in range(1,3):
                    try:
                        station_number = int(row[i])
                        break
                    except ValueError:
                        continue
                station_lookup[station_number] = {'name': row[0], 'latitude':row[i+2],'longitude': row[i+3], 'height':row[i+4], 'networknumber':row[i+1], 'file_identifier': file}
                
            #skip to data    
            while True:
                ready_row = next(csv_reader)        
                if len(ready_row) > 0 and ready_row[0] == 'Station':
                    break            
            
            for idx, row in enumerate(csv_reader):
                if len(row) != 5:
                    print(f"processed {idx} rows from file: {file}")
                    break
                new_row = row + list(station_lookup[int(row[0])].values())
                csv_writer.writerow(new_row)
                flattened_file.flush()



